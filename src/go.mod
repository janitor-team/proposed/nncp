module go.cypherpunks.ru/nncp/v8

require (
	github.com/Arceliar/ironwood v0.0.0-20211125050254-8951369625d0
	github.com/davecgh/go-xdr v0.0.0-20161123171359-e6a2ba005892
	github.com/dustin/go-humanize v1.0.0
	github.com/flynn/noise v1.0.0
	github.com/fsnotify/fsnotify v1.5.1
	github.com/gologme/log v1.3.0
	github.com/google/btree v1.0.1 // indirect
	github.com/gorhill/cronexpr v0.0.0-20180427100037-88b0669f7d75
	github.com/hjson/hjson-go v3.1.0+incompatible
	github.com/klauspost/compress v1.14.4
	github.com/klauspost/cpuid/v2 v2.0.11 // indirect
	github.com/yggdrasil-network/yggdrasil-go v0.4.2
	go.cypherpunks.ru/balloon v1.1.1
	go.cypherpunks.ru/recfile v0.4.3
	golang.org/x/crypto v0.0.0-20220214200702-86341886e292
	golang.org/x/net v0.0.0-20220225172249-27dd8689420f
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/time v0.0.0-20191024005414-555d28b269f0 // indirect
	inet.af/netstack v0.0.0-20211120045802-8aa80cf23d3c
	lukechampine.com/blake3 v1.1.7
)

go 1.13
